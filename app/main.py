import json

import uvicorn
import base64
import pickle
import aiohttp
import tekore as tk
import aio_pika
import asyncio
import uuid
from fastapi import FastAPI, Request, HTTPException, status
from fastapi.responses import RedirectResponse
from starlette.middleware.sessions import SessionMiddleware
from .config import settings, MQ_URL


app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key=settings.session_middleware_key)
cred = tk.Credentials(settings.spotify_client_id,
                      settings.spotify_secret,
                      redirect_uri=f'{settings.host}callback',
                      sender=tk.AsyncSender(),
                      asynchronous=True)
spotify = tk.Spotify()
scope = tk.scope.read
auth = tk.UserAuth(cred, scope)
session = aiohttp.ClientSession()
futures = {}


@app.on_event("startup")
async def startup():
    attempt = 10
    while True:
        try:
            attempt -= 1
            mq_connection = await aio_pika.connect_robust(MQ_URL)
            break
        except ConnectionError:
            if attempt > 0:
                print("Reestablish connection to MQ")
                await asyncio.sleep(10)
            else:
                raise
    channel = await mq_connection.channel()
    callback_queue = await channel.declare_queue(name='music_callback_queue', exclusive=True)
    await callback_queue.consume(on_response)
    print(callback_queue.name)
    music_exchange = await channel.declare_exchange("music", aio_pika.ExchangeType.DIRECT)
    await callback_queue.bind(music_exchange, callback_queue.name)
    setattr(app, 'music_exchange', music_exchange)
    setattr(app, 'callback_queue', callback_queue)


@app.on_event("shutdown")
async def shutdown():
    await session.close()


@app.get("/")
async def root():
    return {"message": "hello world user music"}


@app.get('/spotify_login')
async def spotify_login(request: Request):
    # TODO REMOVE HARDCODE
    request.session[
        "Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhdnNodXNoYXJpbkBnbWFpbC5jb20iLCJpYXQiOjE2NDAwNjU2NzcsIm5iZiI6MTY0MDA2NTY3NywianRpIjoiYjVjYzJiZGMtZWRlMS00ODU1LWJiYzMtN2FhMGNmMzI5YjgxIiwiZXhwIjoxNjQzNjY1Njc3LCJ0eXBlIjoiYWNjZXNzIiwiZnJlc2giOmZhbHNlfQ.CBi8qF8VSZkr5HY74JdvgQwswxqVrCkF6p7LIkAjR6s"
    return RedirectResponse(auth.url)


@app.get('/callback')
async def callback(request: Request, code: str, state: str):
    token = await auth.request_token(code, state)
    async with session.post(f'{settings.user_service}set_token', json={"token": base64.b64encode(pickle.dumps(token)).decode(), 'service': "spotify"}, headers={"Authorization": request.session["Authorization"]}) as resp:
        if resp.status == 200:
            return {"message": "Succesfull spotify registration"}
        else:
            raise HTTPException(status.HTTP_400_BAD_REQUEST)


def on_response(message: aio_pika.IncomingMessage):
    future = futures.pop(message.correlation_id)
    future.set_result(message.body)


@app.post('/game_action', status_code=status.HTTP_200_OK)
async def game_action(request: Request, action_data: dict):
    async with session.get(f'{settings.user_service}get_user', headers={"Authorization": request.headers['authorization']}) as resp:
        if resp.status == 200:
            user = await resp.json()
        else:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, {"user": "unauthorized"})
    correlation_id = str(uuid.uuid4())
    future = asyncio.get_running_loop().create_future()
    futures[correlation_id] = future
    action_data.update({"user": user})
    await app.music_exchange.publish(
        aio_pika.Message(
            json.dumps({"NewGame": action_data, 'action': action_data.pop('action')}).encode(),
            content_type="application/json",
            correlation_id=correlation_id,
            reply_to=app.callback_queue.name,
        ),
        routing_key=settings.music_queue_key,
    )
    future = json.loads(await future)
    if future["result"] == "OK":
        return {"detail": {"message": "Action succeded"}}
    raise HTTPException(status.HTTP_400_BAD_REQUEST, {"message": "Action failed"})


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)


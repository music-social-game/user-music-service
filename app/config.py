from pydantic import BaseSettings
from pathlib import Path

ENV_PATH = Path.cwd() / '.env'


class Settings(BaseSettings):
    host: str
    user_service: str
    spotify_client_id: str
    spotify_secret: str
    session_middleware_key: str
    mq_host:  str
    mq_port:  int
    mq_user:  str
    mq_pass:  str
    music_queue_key:  str

    class Config:
        env_file = ENV_PATH


settings = Settings()

MQ_URL = f"amqp://{settings.mq_user}:{settings.mq_pass}@{settings.mq_host}:{settings.mq_port}/"